<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];


    use HasFactory;

    public function path()
    {
        return route('posts.show', $this);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'id', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'post_id')->orderByDesc('created_at');
    }
}
