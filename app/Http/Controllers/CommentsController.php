<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function index()
    {
        return view('pages/posts/post');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::id();

        request()->validate([
            'body' => ['required','min:3'],
            'post_id' => ['required']
        ]);

        Comment::create([
            'body' => request('body'),
            'user_id' => $userId,
            'post_id' => request('post_id')
        ]);

        return redirect(route('posts.show',request('post_id')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comments)
    {
        $comments->delete();

        return redirect()->back();
    }
}
