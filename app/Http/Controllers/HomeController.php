<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(User $user)
    {
        return view('dashboard', [
            'posts' => $user->find(Auth::id())->posts()->orderByDesc('created_at')->get(),
            'categories' => $user->find(Auth::id())->categories()->get()
        ]);
    }
}
