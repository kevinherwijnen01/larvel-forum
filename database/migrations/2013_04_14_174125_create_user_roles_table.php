<?php

use App\Models\UserRole;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role');
        });

        $this->roleInsert('user', 'admin');
    }

    /**
     * Function to insert roles into the database while migrating
     *
     * @param string ...$roles
     */
    private function roleInsert(string ...$roles)  {
        foreach ($roles as $role) {
            $model = new UserRole();
            $model->setAttribute('role', $role);
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
