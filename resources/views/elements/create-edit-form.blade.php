<form method="POST" action="{{ $data['action'] }}">
    @csrf

    @if( $data['page'] == "create_comment")
        <input type="hidden" name="post_id" value="{{ request()->comments }}">
    @endif

    <input type="hidden" name="_method" value="{{$data['method']}}">

    <input type="hidden" name="user_id" value="{{Auth::id()}}">

    @if( $data['page'] == "create_post" )
        <div class="form-group">
            <label class="" for="title">Chose category</label>
            <div>
                <select class="form-control" name="category_id">
                    <option>Select category</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ @old('category_id',$category->id) == $category->id ? "selected" :""}}>
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif

    @if( $data['page'] != "create_comment" )
        <div class="form-group">
            <label class="" for="title">Title</label>
            <div>
                <input
                    class="form-control {{$errors->has('title') ? 'danger-class' : ''}}"
                    type="text"
                    name="title"
                    id="title"
                    value="{{ @old('title', $data['post']->title) }}" >
                @error('title')
                <p>
                    {{$errors->first('title')}}
                </p>
                @enderror
            </div>
        </div>
    @endif

    <div class="form-group">
        <label class="" for="body">Body</label>
        <div class="">
            <textarea
                class="form-control textarea @error('body') is-danger @enderror"
                name="body"
                id="body"
            >{{@old('body', $data['post']->body)}}</textarea>

            @error('body')
            <p>
                {{$errors->first('body')}}
            </p>
            @enderror
        </div>
    </div>

    <button class="btn btn-success" type="submit">Submit</button>
</form>
