<div class="container footer-container">
    <div class="row">
        <div class="col col-sm-8 col-md-6 col-lg-4 footer">
            <div class="footer-social-media">
                <div>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </div>
                <div>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </div>
                <div>
                    <a href="#"><i class="fab fa-snapchat-ghost"></i></a>
                </div>
                <div>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="copyright">
            <span>
                &copy; Copyright. All rights reserved.
            </span>
            </div>
        </div>
    </div>
</div>
