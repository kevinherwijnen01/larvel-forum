@extends('layout')

@section('title','Create post')

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="edit">
                    <div class="edit-title">
                        <div class="flex-row d-flex">
                            <div class="d-flex">
                                <h2>Create comment</h2>
                            </div>
                        </div>
                    </div>
                    <div class="edit-fields">
                        <?php $data=[
                            'method' => "POST",
                            'action' => route('posts.comments.store'),
                            'page'   => "create_comment"
                        ]
                        ?>
                        @include('elements.create-edit-form', $data)
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
