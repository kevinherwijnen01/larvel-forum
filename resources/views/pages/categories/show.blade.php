@extends('layout')

@section('title','Posts')

@section('page-class',"posts-page")

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container titel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-titel">
                    <h1>Posts</h1>
                </div>
                <div class="create-post">
                    @if (Auth::id())
                        <a href="{{route('posts.create')}}" class="btn btn-success">
                            <span>
                                <i class="far fa-edit"></i>
                                Create post
                            </span>
                        </a>
                    @else
                        <a class="btn btn-secondary" href="{{ route('login') }}">
                            <span>
                                Log in to create your own post.
                            </span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
{{--                @foreach ($categories as $postCategories)--}}
                    <div class="posts">
                        <div class="post">
                            <div class="col-md-3"><i class="fas fa-folder-plus"></i>{{ $categoryName }}</div>
                            <div class="short-description col-md-7"><span>Short discription</span></div>
                            @if (Auth::id())
                                <div class="col-md-1 post-button">Edit</div>
                                <div class="col-md-1 post-button">Delete</div>
                            @endif
                        </div>
{{--                        {!! ddd($posts) !!}--}}
                        @foreach ($posts as $post)
                            <a class="post" href="{{ route('posts.destroy', $post->id)}}">
                                <div class="col-md-3"><i class="fas fa-folder-plus"></i>{{$post->title}}</div>
                                <div class="col-md-7 short-description"><span>{{ str_limit($post->body, 70)}}</span></div>
                                <div class="col-md-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.edit', $post->id)}}" method="get">
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                                <div class="col-md-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </a>
                        @endforeach
                    </div>
{{--                @endforeach--}}
            </div>
        </div>

        <div class="row custom-pagination">
            <!-- PAGINATION -->
            {{ $posts->links() }}
        </div>
    </div>
@endsection
