@extends('layout')

@section('title','Post')

@section('page-class',"post-page")

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
    <link href="/css/comments/default.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container titel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-titel">
                    <h1>{{$post->title}}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="posts">
                    <div class="post d-flex-inline justify-content-between">
                        <div class="col-auto"><i class="fas fa-user-circle"></i>Name of writer</div>
                        @if (Auth::id() == $post->user_id)
                            <div class="d-flex">
                                <div class="col-auto post-button">
                                    <form action="{{ route('posts.edit', $post->id)}}" method="get">
                                        @csrf

                                        <button class="fabutton" type="submit">
                                            <i class="fas fa-edit"></i>
                                            <span class="d-none d-lg-inline-block">Edit post</span>
                                        </button>
                                    </form>
                                </div>
                                <div class="col-auto post-button">
                                    <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit" class="fabutton">
                                            <i class="fas fa-trash"></i>
                                            <span class="d-none d-lg-inline-block">Delete post</span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-12 short-description">
                        <span>
                            {{$post->body}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container comments-container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-titel">
                    <h4>Comments:</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ($post->comments()->where('post_id', $post->id)->with('user')->get() as $comment)
                <div class="comments">
                    <div class="comment d-flex-inline justify-content-between">
                        <div class="col-auto"><i class="fas fa-user-circle"></i>{{$comment->user()->first()->name}}</div>
                        @if (Auth::id() == $comment->user_id)
                            <div class="col-auto post-button">
                                <form action="{{ route('posts.comments.destroy', $comment->id)}}" method="post">
                                    @method('DELETE')
                                    @csrf

                                    <button type="submit" class="fabutton">
                                        <i class="fas fa-trash"></i>
                                        <span class="d-none d-lg-inline-block">Delete comment</span>
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div class="col-12 short-description">
                        <span>
                            {{$comment->body}}
                        </span>
                    </div>
                </div>
                @endforeach

                @if (Auth::id())
                    <form action="{{ route('posts.comments.create', $post->id)}}" method="get">
                        @csrf
                        <button class="btn theme-button" type="submit">
                            <i class="far fa-edit"></i>
                            <span>
                                    Create comment
                                </span>
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection

