@extends('layout')

@section('title','Posts')

@section('page-class',"posts-page")

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container titel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-titel">
                    <h1>Posts</h1>
                </div>
                <div class="create-post">
                    @if (Auth::id())
                        <a href="{{route('posts.create')}}" class="btn theme-button">
                            <span>
                                <i class="far fa-edit"></i>
                                Create post
                            </span>
                        </a>
                    @else
                        <a class="btn theme-button" href="{{ route('login') }}">
                            <span>
                                Log in to create your own post.
                            </span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @foreach ($categories as $postCategories)
                    <div class="posts">
                        <div class="post">
                            <div class="col-lg-3"><i class="fas fa-folder-plus"></i>{{ $postCategories->name }}</div>
                            <div class="d-none d-lg-flex col-lg-7 short-description"><span>Short discription</span></div>
                            @if (Auth::id())
                                <div class="d-none d-lg-flex col-lg-1 post-button">Edit</div>
                                <div class="d-none d-lg-flex col-lg-1 post-button">Delete</div>
                            @endif
                        </div>
                        @foreach ($posts->where('category_id', $postCategories->id)->take(10) as $post)
                            <a class="post" href="{{ route('posts.destroy', $post->id)}}">
                                <div class="col-9 col-lg-3 post-title"><i class="fas fa-folder-plus"></i>{{$post->title}}</div>
                                <div class="col-12 col-lg-7 short-description"><span>{{ str_limit($post->body, 150)}}</span></div>
                                <div class="col-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.edit', $post->id)}}" method="get">
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                                <div class="col-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </a>
                        @endforeach
                        <a href="{{route('category.show', $postCategories->id)}}" class="post category-link">
                            <div class="col-md-12">
                                <i class="fas fa-chevron-circle-right"></i>
                                <span>
                                    See everything of the category: {{ $postCategories->name }}
                                </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="row custom-pagination">
            <!-- PAGINATION -->
            {{ $categories->links() }}
        </div>
    </div>
@endsection
