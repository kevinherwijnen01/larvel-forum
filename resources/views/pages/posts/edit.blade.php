@extends('layout')

@section('title','Edit post')

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="edit">
                <div class="edit-title">
                    <div class="flex-row d-flex">
                        <div class="post-title d-flex">
                            <h2>Edit post</h2>
                        </div>
                    </div>
                </div>
                <div class="edit-fields">
                    <?php $data=[
                        'method' => "PUT",
                        'action' => route('posts.update',$post->id),
                        'page'   => "edit_post",
                        'post'   => $post
                    ]  ?>
                    @include('elements.create-edit-form', $data)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
