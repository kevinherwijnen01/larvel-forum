@extends('layout')

@section('custom-css')
    <link href="/css/posts/default.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container titel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-titel">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Your posts:</h3>
            </div>
        </div>
        @foreach($categories as $category)
            <div class="row">
                <div class="col-md-12">
                    <div class="posts">
                        <div class="post">
                            <div class="col-lg-3"><i class="fas fa-folder-plus"></i>{{ $category->name }}</div>
                            <div class="d-none d-lg-flex col-lg-7 short-description"><span>Short discription</span></div>
                            @if (Auth::id())
                                <div class="d-none d-lg-flex col-lg-1 post-button">Edit</div>
                                <div class="d-none d-lg-flex col-lg-1 post-button">Delete</div>
                            @endif
                        </div>
                        @foreach ($posts->where('category_id', $category->id) as $post)
                            <a class="post" href="{{ $post->path() }}">
                                <div class="col-9 col-lg-3 post-title"><i class="fas fa-folder-plus"></i>{{$post->title}}</div>
                                <div class="col-12 col-lg-7 short-description"><span>{{ str_limit($post->body, 70)}}</span></div>
                                <div class="col-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.edit', $post->id)}}" method="get">
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                                <div class="col-1 post-button">
                                    @if (Auth::id())
                                        <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf

                                            <button type="submit" class="fabutton">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
