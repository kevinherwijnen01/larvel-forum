<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('pages/homepage');
});

Route::get('/contact', function () {
    return view('pages/contact');
});

Route::group(['prefix' => 'posts'], function () {
    Route::get('', [PostsController::class, 'index'])->name('posts.index');
    Route::post('', [PostsController::class, 'store'])->name('posts.store');
    Route::get('create', [PostsController::class, 'create'])->name('posts.create');
    Route::get('{post}', [PostsController::class, 'show'])->name('posts.show');
    Route::get('{post}/edit', [PostsController::class, 'edit'])->name('posts.edit');
    Route::put('{post}', [PostsController::class, 'update'])->name('posts.update');
    Route::delete('{post}', [PostsController::class, 'destroy'])->name('posts.destroy');

    Route::post('comments', [CommentsController::class, 'store'])->name('posts.comments.store');
    Route::get('{comments}/comments', [CommentsController::class, 'create'])->name('posts.comments.create');
    Route::delete('{comments}/comments', [CommentsController::class, 'destroy'])->name('posts.comments.destroy');

    Route::get('categories/{category}', [CategoryController::class, 'show'])->name('category.show');
});

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
});
